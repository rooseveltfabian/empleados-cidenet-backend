  /*****funcion validar si hay datos en la varible *****/
  let isset = (varible) =>{
    if (typeof varible == "undefined" || varible == null) {
      return false;
    } else {
      return true;
    }
  }
  /***************************************************/

  /**
   * fucncion para valida si todas las latras de las cadena son de [A-Z] sin Ñ ni caracteres diferentes
   * @param {*} text 
   * @returns 
   */
  let validaMayusculas = (text)=>{
    let expreg = new RegExp("^[A-Z /S/]+$");
    return (expreg.test(text) || text === "")? true : false;
  }

  /**
   * limpia la cadena para solo permitir un espacio de separacion ej:"FABIAN     OSPINA" => "FABIAN OSPINA"
   * @param {*} cadena 
   * @returns 
   */
  let limpiarEnBlanco = (cadena)=>{
      return ( cadena )? cadena.replace(/\s{2,}/g, ' ').trim() : "";
  } 

  let limpiaCaracteres = (cadena) =>{
    return ( cadena )? cadena.replace(/[^a-zA-ZÀ-ÿ0-9- \u00f1\u00d1]/g, '').trim() : ""; //solo permite los caracteres especificados en la expresion
  }

 /**
   * funcion que valida la expresion regular que permite solo letras en mayuculas o minusculas o numeros
   * @param {*} text 
   * @returns 
   */
  let validaIdentificacion = (text)=>{
    let expreg = new RegExp("^[A-Za-z0-9-]+$");
    return (expreg.test(text))? true : false;
  }  

  module.exports = {
      isset,
      validaMayusculas,
      limpiarEnBlanco,
      validaIdentificacion,
      limpiaCaracteres     
  }