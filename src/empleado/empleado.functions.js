var { getConn } = require('../mysql/connection');
var { limpiaCaracteres, limpiarEnBlanco } = require('../global-functions/general.functions');


let getAllEmployeesFun = () => {
    return new Promise((resolve, reject) => {
        getConn().select('*').from('empleado')
            .then(
                data => resolve(data),
                error => reject(error)
            );
    });
}

let getAllEmployeesPageFun = (indice) => {
    return new Promise((resolve, reject) => {
        let limite = 10;
        let cantidad = indice * limite;        
        let sql ="SELECT e.*, p.nombre as pais_empleo,  t.nombre as tipo_identificacion, a.nombre as area FROM empleado e, pais p, tipo_identificacion t, area a WHERE e.id_pais_empleo=p.id && e.id_tipo_identificacion=t.id && a.id=e.id_area ORDER BY e.id LIMIT " + limite + " OFFSET "+ cantidad +"";
        getConn().raw(sql)
            .then(
                data => resolve(data[0]),
                error => reject(error)
            )
        });
}

let insertEmployeeFun = (
    primer_nombre, 
    otros_nombres, 
    primer_apellido, 
    segundo_apellido, 
    no_identificacion,
    id_tipo_identificacion,
    id_pais_empleo,
    fecha_ingreso,
    id_area,
    fecha_hora_registro) => {
    return new Promise((resolve, reject) => {
/*         let usuario = primer_nombre.toLowerCase().replace(/\s+/g, '')+"."+primer_apellido.toLowerCase().replace(/\s+/g, '');
        let dominio = id_pais_empleo == 1? "@cidenet.com.co" : "@cidenet.com.us";
        let correoDef = usuario + dominio; 

        //consulto si hay correos con la forma <usuario>%<dominio> ej: fabian.ospina%@cidenet.com.co
        getConn().select('*').from('empleado').where('correo', 'like', usuario+'%'+ dominio)
        .then((empleados) => {
            console.log("empleados: ",empleados);
            if(empleados.length === 0){ // si el correo no existe en la base de datos, lo inserto
                return getConn()('empleado').insert({ primer_nombre,otros_nombres, segundo_apellido, primer_apellido , no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area, fecha_hora_registro, correo: correoDef });
            }else{// si hay correos similares en la base de datos. Ej:(fabian.ospina@cidenet.com.co, fabian.ospina.2@cidenet.com.co)
                let correoAlt = "";
                for(let i = 0; i< empleados.length + 1; i++){ //genero una serie de correos alternos Ej(fabian.ospina@cidenet.com.co, fabian.ospina.1@cidenet.com.co) y los comparo con los que ya existe. cuando encuentro uno que no existe en la base de datos elijo ese para insertarlo
                    let ext = i > 0? '.'+i : '';
                    let res = empleados.filter( em => em.correo === (usuario + ext + dominio))
                    if(res.length === 0){
                        correoAlt = (usuario + ext + dominio);
                        break;
                    }
                }
                console.log(correoAlt)
                //inserto el empleado
                return getConn()('empleado').insert({ primer_nombre, otros_nombres, segundo_apellido, primer_apellido , no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area, fecha_hora_registro, correo: correoAlt });
            }

        })
        .then(()=> resolve())
        .catch(err => reject(err))
 */

        genEmail(primer_nombre, primer_apellido, id_pais_empleo )
        .then( correo =>{
            return getConn()('empleado').insert({ primer_nombre, otros_nombres, segundo_apellido, primer_apellido , no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area, fecha_hora_registro, correo });
        })
        .then(()=> resolve())
        .catch(err => reject(err))
    });
}

/**
 * 
 * @param {*} no_identificacion 
 * @param {*} id_tipo_identificacion 
 * @param {*} id opcional: un id a excluir en la consulta
 * @returns 
 */
let getEmployeesRepeatedForIdentFun = ( no_identificacion, id_tipo_identificacion, id = null ) =>{
    return new Promise((resolve, reject) => {
        let sql = "";
        if(id){
            sql ="SELECT COUNT(*) FROM empleado WHERE (no_identificacion='"+no_identificacion+"' AND id_tipo_identificacion= "+id_tipo_identificacion+") && id !="+id+"";
        } else{
            sql ="SELECT COUNT(*) FROM empleado WHERE (no_identificacion='"+no_identificacion+"' AND id_tipo_identificacion= "+id_tipo_identificacion+")" ;
        }     
    getConn().raw(sql)
        .then(
            data => resolve(parseInt(data[0][0]['COUNT(*)'])),
            error => reject(error)
        )
    });
}


let filterEmployeesFun = (consulta)=>{
    return new Promise((resolve, reject) => {  
        consulta = limpiaCaracteres( limpiarEnBlanco(consulta) );   
        let sql ="SELECT e.*, p.nombre as pais_empleo, t.nombre as tipo_identificacion, a.nombre as area FROM empleado e, tipo_identificacion t, pais p, area a WHERE e.id_tipo_identificacion = t.id && p.id=e.id_pais_empleo && a.id=e.id_area && ( "+
                    "e.primer_nombre LIKE '%"+consulta+"%' OR "+
                    "e.otros_nombres LIKE '%"+consulta+"%' OR "+
                    "e.primer_apellido LIKE '%"+consulta+"%' OR "+
                    "e.segundo_apellido LIKE '%"+consulta+"%' OR "+
                    "t.nombre LIKE '%"+consulta+"%' OR "+
                    "e.no_identificacion LIKE '%"+consulta+"%' OR "+
                    "p.nombre LIKE '%"+consulta+"%' OR "+
                    "e.correo LIKE '%"+consulta+"%' OR "+
                    "e.estado LIKE '%"+consulta+"%' ) ";
        getConn().raw(sql)
            .then(
                data => resolve(data[0]),
                error => reject(error)
            )
        });
}


let editEmployeesFun = ( 
    id, 
    primer_nombre, 
    otros_nombres, 
    primer_apellido, 
    segundo_apellido, 
    no_identificacion, 
    id_tipo_identificacion, 
    id_pais_empleo, 
    fecha_ingreso,
    id_area,
    fecha_edicion
)=>{
    return new Promise((resolve, reject) => {
        //consulto el empleado con el id
        getEmployeeForId(id) 
        .then(empleado =>{
            if(empleado.length === 1){//si el id es correcto y existe el empleado
                // si se ha modificado el primer_nombre o el primer_apellido
                if(empleado[0].primer_nombre !== primer_nombre || empleado[0].primer_apellido !== primer_apellido){
                    let nuevoCorreo = null;
                    return genEmail(primer_nombre, primer_apellido, id_pais_empleo ) //genero un corre valido
                        .then( correo =>{
                            nuevoCorreo = correo;
                            return getConn()('empleado').where('id', id)
                                    .update({
                                        primer_nombre, 
                                        otros_nombres, 
                                        primer_apellido, 
                                        segundo_apellido, 
                                        no_identificacion, 
                                        id_tipo_identificacion, 
                                        id_pais_empleo, 
                                        fecha_ingreso,
                                        id_area,
                                        fecha_edicion,
                                        correo
                                    })
                        })
                        .then((res)=> {
                            console.log(res)
                            resolve(nuevoCorreo)
                        })
                        .catch(err => reject(err))                    
                }else{
                    // si no se ha modificado el primer_nombre o el primer_apellido => actualizo
                    return getConn()('empleado').where('id', id)
                    .update({
                        primer_nombre, 
                        otros_nombres, 
                        primer_apellido, 
                        segundo_apellido, 
                        no_identificacion, 
                        id_tipo_identificacion, 
                        id_pais_empleo,
                        fecha_ingreso, 
                        id_area,
                        fecha_edicion
                    })
                    .then(()=> resolve())
                    .catch(err => reject(err))
                }
            }else{
                //si el id NO es correcto y NO existe el empleado
                reject("There_Is_No_Employee");
            }
        })
        .catch(err => reject(err))
    })
}

let getEmployeeForId = ( id )=>{
    return new Promise((resolve, reject) => {
        getConn()('empleado').where('id', id)
        .then(
            data => resolve(data),
            error => reject(error)
        );
    })
}


let genEmail = (primer_nombre, primer_apellido, id_pais_empleo )=>{
    return new Promise((resolve, reject) => {
        let usuario = primer_nombre.toLowerCase().replace(/\s+/g, '')+"."+primer_apellido.toLowerCase().replace(/\s+/g, '');
        let dominio = id_pais_empleo == 1? "@cidenet.com.co" : "@cidenet.com.us";
        let correoDef = usuario + dominio; 

        //consulto si hay correos con la forma <usuario>%<dominio> ej: fabian.ospina%@cidenet.com.co
        getConn().select('*').from('empleado').where('correo', 'like', usuario+'%'+ dominio)
        .then((empleados) => {
            console.log("empleados: ",empleados);
            if(empleados.length === 0){ // si el correo no existe en la base de datos, lo inserto
                resolve(correoDef);
            }else{// si hay correos similares en la base de datos. Ej:(fabian.ospina@cidenet.com.co, fabian.ospina.2@cidenet.com.co)
                let correoAlt = "";
                for(let i = 0; i< empleados.length + 1; i++){ //genero una serie de correos alternos Ej(fabian.ospina@cidenet.com.co, fabian.ospina.1@cidenet.com.co) y los comparo con los que ya existe. cuando encuentro uno que no existe en la base de datos elijo ese para insertarlo
                    let ext = i > 0? '.'+i : '';
                    let res = empleados.filter( em => em.correo === (usuario + ext + dominio))
                    if(res.length === 0){
                        correoAlt = (usuario + ext + dominio);
                        break;
                    }
                }
                resolve(correoAlt);
            }

        })
        .catch(err => reject(err))
    })
}

let deleteEmployeeFun = (id)=>{
    return new Promise((resolve, reject) => {
        getConn()('empleado').where('id', id).del()
        .then(
            () => resolve(),
            err => reject(err)
        )
    })
}

let countAllEmployeesFun = ()=>{
    return new Promise((resolve, reject)=>{
        getConn().count('*').from('empleado')
        .then(
            data => resolve(data[0]["count(*)"]),
            err => reject(err)
        )
    })
}


module.exports = {
    getAllEmployeesFun,
    getAllEmployeesPageFun,
    insertEmployeeFun,
    getEmployeesRepeatedForIdentFun,
    filterEmployeesFun,
    editEmployeesFun,
    deleteEmployeeFun,
    countAllEmployeesFun
}