var { isset, limpiarEnBlanco, validaMayusculas, validaIdentificacion } = require('../global-functions/general.functions');
var {  countAllEmployeesFun, getAllEmployeesFun, getAllEmployeesPageFun, insertEmployeeFun, getEmployeesRepeatedForIdentFun, filterEmployeesFun, editEmployeesFun, deleteEmployeeFun } = require('./empleado.functions');
var moment = require('moment');

let getAllEmployees = async (req, res, next) => {
    try {
        var empleados = await getAllEmployeesFun();
    }
    catch (error) {
        return res.json({
            status: false
        });
    }
    res.json({
        status: true,
        empleados
    })
}

let getAllEmployeesPage = async (req, res, next) => {
    if (isset(req.query.indice)) {
        try {
            var empleados = await getAllEmployeesPageFun(req.query.indice);
        }
        catch (error) {
            return res.json({
                status: false
            });
        }
        res.json({
            status: true,
            empleados
        })
    } else {
        return res.json({
            status: false,
            msg: 'Missing_Data'
        })
    }
}


let countAllEmployees = async (req, res, next) => {
        try {
            var cantidad = await countAllEmployeesFun();
        }
        catch (error) {
            return res.json({
                status: false
            });
        }
        res.json({
            status: true,
            cantidad
        })
}




let insertEmployee = async (req, res) => {
    //valido que siempre reciba todos los datos necesarios

    if (
        isset(req.body.primer_nombre) &&
        isset(req.body.primer_apellido) &&
        isset(req.body.no_identificacion) &&
        isset(req.body.id_tipo_identificacion) &&
        isset(req.body.id_pais_empleo) &&
        isset(req.body.id_area) &&
        isset(req.body.fecha_ingreso)
    ) {
        let { primer_nombre, otros_nombres, primer_apellido, segundo_apellido, no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area } = req.body;
        primer_nombre = limpiarEnBlanco(primer_nombre); //limpio las cadenas de los espacios en blanco
        otros_nombres = limpiarEnBlanco(otros_nombres);
        primer_apellido = limpiarEnBlanco(primer_apellido);
        segundo_apellido = limpiarEnBlanco(segundo_apellido);

        //valido que los datos se recibe correctamenten segun las especificaciones
        if (validaMayusculas(primer_nombre) &&
            validaMayusculas(primer_apellido) &&
            validaMayusculas(otros_nombres) &&
            validaMayusculas(segundo_apellido) &&
            validaIdentificacion(no_identificacion)
        ) {
            let hoy = moment().format("YYYY-MM-DD");
            let fecha_ing = moment(fecha_ingreso.trim(), "YYYY-MM-DD")
            let diasDiferencia = parseInt(fecha_ing.diff(hoy, "days"));
            //valido que la fecha de ingreso no sea inferior a 30 dias atras o superior a la fecha de hoy
            if (fecha_ingreso && (diasDiferencia < -30 || diasDiferencia > 0)) {
                return res.json({
                    status: false,
                    msg: 'Invalid_Date'
                });
            }
            try {
                //valido si hay otro empleado con el mismo numero de identificacion y tipo de identificacion
                var repetidosPorIden = await getEmployeesRepeatedForIdentFun(no_identificacion, id_tipo_identificacion)
            } catch (error) {
                return res.json({
                    status: false,
                    error
                });
            }

            if (repetidosPorIden === 0) {// si no hay identificaciones identicas en la base de datos
                try {
                    var fecha_hora_registro = moment().format("DD/MM/YYYY HH:mm:ss"); //fecha y hora de registro
                    //envio a insertar los datos
                    await insertEmployeeFun(primer_nombre, otros_nombres, primer_apellido, segundo_apellido, no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area, fecha_hora_registro);
                }
                catch (error) {
                    return res.json({
                        status: false,
                        error
                    });
                }
                res.json({
                    status: true
                })
            } else {
                return res.json({
                    status: false,
                    msg: 'Duplicate_Identification'
                })
            }
        } else {
            return res.json({
                status: false,
                msg: 'Data_Error'
            })
        }
    } else {
        return res.json({
            status: false,
            msg: 'Missing_Data'
        })
    }
}

let filterEmployees = async (req, res, next) => {
    if (isset(req.query.consulta)) {
        try {
            var empleados = await filterEmployeesFun(req.query.consulta);
        }
        catch (error) {
            return res.json({
                status: false
            });
        }
        res.json({
            status: true,
            empleados
        })
    } else {
        return res.json({
            status: false,
            msg: 'Missing_Data'
        })
    }
}


let editEmployees = async (req, res, next) => {
    if (
        isset(req.body.id) &&
        isset(req.body.primer_nombre) &&
        isset(req.body.primer_apellido) &&
        isset(req.body.no_identificacion) &&
        isset(req.body.id_tipo_identificacion) &&
        isset(req.body.id_pais_empleo) &&
        isset(req.body.id_area) &&
        isset(req.body.fecha_ingreso)
    ) {
        let { id, primer_nombre, otros_nombres, primer_apellido, segundo_apellido, no_identificacion, id_tipo_identificacion, id_pais_empleo, fecha_ingreso, id_area } = req.body;

        primer_nombre = limpiarEnBlanco(primer_nombre); //limpio las cadenas de los espacios en blanco
        otros_nombres = limpiarEnBlanco(otros_nombres);
        primer_apellido = limpiarEnBlanco(primer_apellido);
        segundo_apellido = limpiarEnBlanco(segundo_apellido);

        //valido que los datos se recibe correctamenten segun las especificaciones
        if (
            validaMayusculas(primer_nombre) &&
            validaMayusculas(primer_apellido) &&
            validaMayusculas(otros_nombres) &&
            validaMayusculas(segundo_apellido) &&
            validaIdentificacion(no_identificacion)
        ) {
            let hoy = moment().format("YYYY-MM-DD");
            let fecha_ing = moment(fecha_ingreso.trim(), "YYYY-MM-DD")
            let diasDiferencia = parseInt(fecha_ing.diff(hoy, "days"));
            //valido que la fecha de ingreso no sea inferior a 30 dias atras o superior a la fecha de hoy
            if (fecha_ingreso && (diasDiferencia < -30 || diasDiferencia > 0)) {
                return res.json({
                    status: false,
                    msg: 'Invalid_Date'
                });
            }

            try {
                //valido si hay otro empleado con el mismo numero de identificacion y tipo de identificacion diferente al empleado editado
                var repetidosPorIden = await getEmployeesRepeatedForIdentFun(no_identificacion, id_tipo_identificacion, id)
            } catch (error) {
                return res.json({
                    status: false,
                    error
                });
            }

            if (repetidosPorIden === 0) {// si no hay identificaciones identicas en la base de datos

                try {
                    let fecha_edicion = moment().format("DD/MM/YYYY HH:mm:ss"); //fecha y hora de actualizacion
                    var correo = await editEmployeesFun(
                        id,
                        primer_nombre,
                        otros_nombres,
                        primer_apellido,
                        segundo_apellido,
                        no_identificacion,
                        id_tipo_identificacion,
                        id_pais_empleo,
                        fecha_ingreso,
                        id_area,
                        fecha_edicion
                    );
    
                }
                catch (error) {
                    return res.json({
                        status: false
                    });
                }

                res.json({
                    status: true,
                    correo
                })

            } else {
                return res.json({
                    status: false,
                    msg: 'Duplicate_Identification'
                })
            }
        } else {
            return res.json({
                status: false,
                msg: 'Data_Error'
            })
        }

    } else {
        return res.json({
            status: false,
            msg: 'Missing_Data'
        })
    }
}

let deleteEmployee = async (req, res, next) => {
    if (isset(req.query.id)) {
        try {
            await deleteEmployeeFun(req.query.id);
        }
        catch (error) {
            return res.json({
                status: false
            });
        }
        res.json({
            status: true
        })
    } else {
        return res.json({
            status: false,
            msg: 'Missing_Data'
        })
    }
}

module.exports = {
    getAllEmployees,
    getAllEmployeesPage,
    insertEmployee,
    filterEmployees,
    editEmployees,
    deleteEmployee,
    countAllEmployees
}
