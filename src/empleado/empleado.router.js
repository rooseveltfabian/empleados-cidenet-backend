var { countAllEmployees, getAllEmployees, getAllEmployeesPage, insertEmployee, filterEmployees, editEmployees, deleteEmployee } = require('./empleado.response');

module.exports = function (app) {
    //ruta para listar todos lo empleados
    app.get('/empleados/listar', getAllEmployees);
    //ruta para listar todos los empleados consultando de 10 en 10 segun el indice recibido
    app.get('/empleados/listar/pagina', getAllEmployeesPage);
    //ruta para inserta el empleado 
    app.post('/empleados/insertar', insertEmployee );
    //ruta para filtar los empleado, recibe una consulta y el indice de pagina
    app.get('/empleados/filtrar', filterEmployees );
    //ruta para editar un empleado
    app.put('/empleados/editar', editEmployees );
    //ruta para eliminar un empleado 
    app.delete('/empleados/eliminar', deleteEmployee );
    //ruta apara consultar la cantidad de empleados
    app.get('/empleados/cantidad', countAllEmployees );
}