
/**
 * funcion middleware que se muestra cuando la ruta consultada no existe
 */
function resolve(req, res, next) {
    res.status(404).json({
      status: false,
      msg: 'Not_Found'
    })
    next();
  }

  module.exports = {
    resolve
}