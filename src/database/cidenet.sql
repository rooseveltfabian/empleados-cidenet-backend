-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2022 a las 18:39:42
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`id`, `nombre`) VALUES
(1, 'Administración'),
(2, 'Financiera'),
(3, 'Compras'),
(4, 'Infraestructura'),
(5, 'Operación'),
(6, 'Talento Humano'),
(7, 'Servicios Varios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `primer_apellido` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_apellido` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primer_nombre` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otros_nombres` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pais_empleo` int(4) NOT NULL,
  `id_tipo_identificacion` int(4) NOT NULL,
  `no_identificacion` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_ingreso` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_area` int(4) NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activo',
  `fecha_hora_registro` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_edicion` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `primer_apellido`, `segundo_apellido`, `primer_nombre`, `otros_nombres`, `id_pais_empleo`, `id_tipo_identificacion`, `no_identificacion`, `correo`, `fecha_ingreso`, `id_area`, `estado`, `fecha_hora_registro`, `fecha_edicion`) VALUES
(33, 'RODRIGUEZ', '', 'ANDRES', '', 1, 1, '1116000111', 'andres.rodriguez@cidenet.com.co', '2022-02-21', 1, 'activo', '21/02/2022 11:03:43', NULL),
(34, 'ACEVEDO', '', 'MIGUEL', '', 1, 1, '1116000222', 'miguel.acevedo@cidenet.com.co', '2022-02-21', 2, 'activo', '21/02/2022 11:05:32', NULL),
(35, 'MONTOYA', '', 'CAMILO', '', 2, 1, '1116000333', 'camilo.montoya@cidenet.com.us', '2022-02-21', 3, 'activo', '21/02/2022 11:06:22', NULL),
(36, 'GALLEGO', '', 'DIANA', '', 1, 1, '1116000444', 'diana.gallego@cidenet.com.co', '2022-02-21', 5, 'activo', '21/02/2022 11:07:04', NULL),
(37, 'MAYA', '', 'PEDRO', '', 1, 1, '1116000555', 'pedro.maya@cidenet.com.co', '2022-02-21', 2, 'activo', '21/02/2022 11:07:33', NULL),
(38, 'MORALES', '', 'CAMILO', '', 1, 1, '1116000666', 'camilo.morales@cidenet.com.co', '2022-02-21', 1, 'activo', '21/02/2022 11:08:15', NULL),
(39, 'MORALES', '', 'CAMILO', '', 2, 2, '1116000666', 'camilo.morales@cidenet.com.us', '2022-02-21', 1, 'activo', '21/02/2022 11:08:51', NULL),
(40, 'MORALES', '', 'CAMILO', 'ANDRES', 1, 1, '1116000777', 'camilo.morales.1@cidenet.com.co', '2022-02-21', 1, 'activo', '21/02/2022 11:10:04', NULL),
(41, 'RUIZ', '', 'RAUL', '', 1, 1, '1116000888', 'raul.ruiz@cidenet.com.co', '2022-02-21', 2, 'activo', '21/02/2022 11:11:53', NULL),
(42, 'ORTIZ', '', 'LINA', '', 1, 1, '1116000999', 'lina.ortiz@cidenet.com.co', '2022-02-21', 6, 'activo', '21/02/2022 11:12:36', NULL),
(43, 'POSADA', '', 'SARA', '', 1, 1, '1116001111', 'sara.posada@cidenet.com.co', '2022-02-21', 1, 'activo', '21/02/2022 11:13:22', NULL),
(44, 'SIERRA', '', 'ANDRES', '', 1, 1, '1116001222', 'andres.sierra@cidenet.com.co', '2022-02-21', 7, 'activo', '21/02/2022 11:14:10', NULL),
(45, 'VALLEJO', '', 'ISABEL', '', 1, 1, '1116001333', 'isabel.vallejo@cidenet.com.co', '2022-02-21', 3, 'activo', '21/02/2022 11:15:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`) VALUES
(1, 'Colombia'),
(2, 'Estados Unidos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_identificacion`
--

CREATE TABLE `tipo_identificacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_identificacion`
--

INSERT INTO `tipo_identificacion` (`id`, `nombre`) VALUES
(1, 'Cédula de Ciudadanía'),
(2, 'Cédula de Extranjería'),
(3, 'Pasaporte'),
(4, 'Permiso Especial');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;