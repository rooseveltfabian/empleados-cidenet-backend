var fs = require('fs');
var { getConnVerificar, getConn } = require('../mysql/connection');



let verificaDataBase = ()=>{
    return getConnVerificar().raw("select SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'cidenet'")
           .then((res)=>{
             if(res[0].length > 0){
               console.log("-> Ya existe la base de datos")
             }else{
               console.log("-> NO existe la base de datos")
               var sql = fs.readFileSync("./src/database/cidenet.sql",{encoding: 'utf8'})
               return getConnVerificar()
                     .raw('CREATE DATABASE cidenet')
                     .then(()=> console.log("-> Base de Datos Creada"))
                     .then(() => getConn().raw(sql))
                     .then(()=> console.log("-> Tablas y Datos de Prueba Insertados"))
               }
             
           })
   
   
             
   }

   module.exports = {
    verificaDataBase
   }