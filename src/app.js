var express = require('express');
var app = express();
require('dotenv').config();
var notfund  = require( './middlewares/notfound');
var { verificaDataBase } = require('./database/dataBase');



/***bodyParser para obtener parametros  */
var bodyParser = require('body-parser');
// parse application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

/*************************************************/


/******** habilita cors para dara acceso ********/
var cors = require('cors');
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", 
  "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
/*************************************************/



//importo la lista de rutas de la app
require('./empleado/empleado.router')(app);



/*******mensaje por defecto cuando no existe
         alguna de las rutas anteriores  *********/
app.use(notfund.resolve);
/*************************************************/



/***** configura el puerto y corre servidor ******/
app.listen(process.env.PORT, function () {
  console.log("Running server in port "+ process.env.PORT);
});
/*************************************************/

/** verifica si existe la base datos. de no existir entonces la crea */
verificaDataBase();