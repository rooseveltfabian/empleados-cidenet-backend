var knex = require('knex');

let conn = undefined;
//funcion para crear la conexion con la base de datos
let createConection = () => {
    const conexion = {
        client: process.env.DB_CLIENT,
        connection: {
            user: process.env.DB_USER,
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            database: process.env.DB_DATABASE,
            password: process.env.DB_PASS,
            multipleStatements: true
        },
        pool: { min: 1, max: 5 }
    };
    conn = knex(conexion); 
    return conn;
}
//retorna la conexion con la base de datos
let getConn = () => {
    
    if ( conn ){
        return conn;
    }
    return createConection();
}

let getConnVerificar = ()=>{
    const conexion = {
        client: process.env.DB_CLIENT,
        connection: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,

            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            multipleStatements: true
        },
        pool: { min: 1, max: 5 }
    };
    let conn2 = knex(conexion);
    return conn2
}

module.exports = {
    getConn,
    getConnVerificar
}